// New VueJS instance
new Vue({

  // CSS selector of the root DOM element
  el: '#notebook',

  // Some data
  data () {
    return {
		  notes: [],
		  // ID of the selected note
		  selectedId: null,
    }
  },
  computed: {
    notePreview () {
      // Markdown rendered to HTML
      return this.selectedNote ? marked(this.selectedNote.content) : ''
    },
    addButtonTitle () {
      return this.notes.length + ' note(s) already'
    },
    selectedNote () {
      // We return the matching note with selectedId
      return this.notes.find(note => note.id === this.selectedId)
    }
  },
  methods: {
    reportOperation (opName) {
      console.log('The', opName, 'operation was completed!')
    },
    addNote () {
      const time = Date.now()
      // Default new note
      const note = {
        id: String(time),
        title: 'New note ' + (this.notes.length +1),
        content: '**Hi!** This notebook is using [markdown](https://github.com/adam-p/markdown-here/Markdown-Cheatsheet) for formatting!',
        created: time,
        favourite: false,
      }
      // Add to the list
      this.notes.push(note)
    },
    selectNote (note) {
      this.selectedId = note.id
    },
  },
  // This will be called when the instance is ready
  created () {
    // Set the content to the stored value
    // or to a default string if nothing was saved
    this.notes.pushcontent = localStorage.getItem('content') || 'You can write in **markdown**!'
  }
})
